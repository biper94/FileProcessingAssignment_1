/**
|--------------------------------------------------
|	This Code is written on Visual Studio 2017.
|	When you build and compile with another IDE, 
|	there might be error.
|--------------------------------------------------
*/
#include <iostream>
#include <io.h>
#include <windows.h>
#include <time.h>
#include <fstream>
#include <tchar.h>
#include <clocale>
#include <cstring>
#include <sstream>
#define MAX_BUF 512

using namespace std;

bool changed = false;

string gLogBuffer[MAX_BUF];
int gLogIndex = 0;

void createLogFile() {

	ofstream fp;
	TCHAR pathBuf[MAX_PATH];

	GetCurrentDirectory(MAX_PATH, pathBuf);

	fp.open("Update.log", ios::app);


	for (int i = 0; i < gLogIndex; i++) {
		fp << gLogBuffer[i].c_str() << endl;
	}

	cout << "Log File created at current folder\n\t";
	wcout << pathBuf << endl;

	fp.close();
}


void addLogBuffer(string dest) {
	time_t currentTime = time(0);
	struct tm *curr_tm;

	string logBuf;
	stringstream ss;


	curr_tm = localtime(&currentTime);
	string curTimeStr;
	int year = curr_tm->tm_year + 1900;	int mon = curr_tm->tm_mon + 1; int day = curr_tm->tm_mday;
	int hour = curr_tm->tm_hour; int min = curr_tm->tm_min; int sec = curr_tm->tm_sec;
	curTimeStr = to_string(year) + "/" + to_string(curr_tm->tm_mon + 1) + "/" + to_string(curr_tm->tm_mday)
		+ " " + to_string(curr_tm->tm_hour) + ":" + to_string(curr_tm->tm_min) + ":" + to_string(curr_tm->tm_sec);
	//cout << curr_tm->tm_year + 1900 << endl;

	TCHAR pathBuf[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, pathBuf);
	if (gLogIndex == MAX_BUF - 1) {// Buffer is full
		cout << "Log Buffer is Full(Max: 512)" << endl;
		createLogFile();
		gLogIndex = 0;
	}

	logBuf = dest + string(" has been updated at ") + curTimeStr;
	gLogBuffer[gLogIndex++] = logBuf;


}



bool isNotExist(string dest) {
	struct _finddata_t fd;
	long handle;

	handle = _findfirst(dest.c_str(), &fd);

	if (handle == -1) {

		return true;
	}
	else return false;
}


bool checkUpdate(string src, string dest) {
	struct _finddata_t fd_src;
	struct _finddata_t fd_dest;
	long handle_src;
	long handle_dest;

	handle_dest = _findfirst(dest.c_str(), &fd_dest);
	handle_src = _findfirst(src.c_str(), &fd_src);



	time_t time_src = fd_src.time_write;
	time_t time_dest = fd_dest.time_write;
	//cout << "SRC ACCESS TIME: " << time_src << endl;
	//cout << "DEST ACCESS TIME: " << time_dest << endl;

	if (difftime(time_src, time_dest) > 0) {
		//cout << endl;

		return true;
	}

	return false;

}


void makeDir(string dest) {
	wchar_t destWCHAR[MAX_PATH];
	mbstowcs(destWCHAR, dest.c_str(), dest.length() + 1);
	CreateDirectory(destWCHAR, NULL);
	
}


void copyFile(string src, string dest) {

	wchar_t srcWCHAR[MAX_PATH];
	wchar_t destWCHAR[MAX_PATH];

	mbstowcs(srcWCHAR, src.c_str(), src.length() + 1);
	mbstowcs(destWCHAR, dest.c_str(), dest.length() + 1);

	int result = CopyFile(srcWCHAR, destWCHAR, 0);

	if (!result) {
		_tprintf(TEXT("\n\n\t\tsrc: %ws\n"), srcWCHAR);
		_tprintf(TEXT("\t\tdest: %ws\n"), destWCHAR);
		cerr << "Copying files Error code: " << GetLastError() << endl;
		if (GetLastError() == 2) {
			cout << "Are there any \"Not English\" path..?\n\n" << endl;
		}
	}

	else {
		
		addLogBuffer(dest);
		changed = true;
		//cout << dest.c_str() << " **Updated**" << endl;
	}


}

void intoDirectory(string srcDir, string destDir) {

	long handle;
	struct _finddata_t fd;
	int result;
	string curDirFiles = srcDir + string("\\*");


	handle = _findfirst(curDirFiles.c_str(), &fd);

	if (handle == -1)
		return;

	result = _findnext(handle, &fd);

	do {
		if (fd.attrib == _A_SUBDIR) { // File is a Directory
			if ((strcmp(fd.name, ".") != 0 && strcmp(fd.name, "..") != 0)) {

				string nextDir = srcDir + string("\\") + fd.name;

				string src = (srcDir + string("\\") + string(fd.name));
				string dest = (destDir + string("\\") + string(fd.name));
				if (/*checkUpdate(src, dest) || */isNotExist(dest))
					makeDir(dest);

				intoDirectory(nextDir, (destDir + "\\" + fd.name));

			}
		}
		else {
			// File is not Directory

			string src = (srcDir + string("\\") + string(fd.name));
			string dest = (destDir + string("\\") + string(fd.name));
			if (checkUpdate(src, dest) || isNotExist(dest))
				copyFile(src, dest);

		}
		result = _findnext(handle, &fd);
	} while (result != -1);

	_findclose(handle);

}
void makeDestDir(string dest) {
	long handle;
	struct _finddata_t fd;

	handle = _findfirst(dest.c_str(), &fd);
	if (handle == -1) {
		for (int i = dest.length() - 1; i >= 0; i--) {
			if (dest[i] == '\\') {
				makeDestDir(dest.substr(0, i));
				makeDir(dest);
				return;
			}
		}
	} 
	else return;

}

int main(int argc, char** argv) {

	string srcDir, destDir;
	if (argc != 3) {
		cerr << "\nUsage: " << argv[0] << " " << "<src directory> <dest directory>" << endl;
		return -1;
	}

	cout << "Source Dir:\t" << argv[1] << endl;
	cout << "Dest Dir:\t" << argv[2] << endl;

	srcDir = string(argv[1]);
	destDir = string(argv[2]);

	makeDestDir(destDir);
	intoDirectory(srcDir, destDir);


	if (changed) {
		createLogFile();
		cout << "\n\nUpdated." << endl;
	}
	else
		cout << "\n\nNothing updated." << endl;

	return 0;

}